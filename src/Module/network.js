import nativeFetch from '@system.fetch'
import prompt from '@system.prompt'

// 服务器地址
const SERVER_URL = 'http://120.27.183.6/';

const network = {
  /**
   * 网络请求
   * @param options
   * @return {Promise}
   */
  async fetch (obj) {
    console.log('开始网络请求');
    const url = SERVER_URL + obj.url;
    let options = Object.assign(obj, {url})
    console.log("参数options:"+options.url);
    const p = new Promise((resolve, reject) => {
      
      options.success = function ({data, code}) {
        console.log("fetch success"+data);
          // 根据后端接口返回来定制。
          // 根据code 来进行拦截
          if (code == 404) {
            prompt.showToast({
                message:'找不到页面'
            })
          }
        resolve( data )
      }
      options.fail = function (data) {
          console.log(data, 'fail');
          // fail 会返回一个错误的字符串说明
          prompt.showToast({
            message:'网络连接失败，请检查网络！'
        })
        resolve( null )
      }
      console.log('options:'+JSON.stringify(options));
      nativeFetch.fetch(options)
      console.log("fetch finish");
    })
    return p
  },
  get (url, data) {
    return this.fetch({url, data});
  },
  post (url, data) {
    return this.fetch({url, data, method: 'post'})
  }
}

// 注入到全局
const hookTo = global.__proto__ || global
hookTo.network = network


